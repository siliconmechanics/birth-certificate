from __future__ import print_function
from fpdf import FPDF
from datetime import date
import csv
import xlsxwriter


# =======================================================================================
# Excel Writer
# Supporting class to __xlsx_sys_report__() method
# =======================================================================================

class ExcelWriter:
    def __init__(self):
        self.loc = {'x':0, 'y':0} # placeholder to track the current location of where to write in the excel doc

    def addLine(self, worksheet, cells, cell_format):
        """
        Adds a line of data to the worksheet. Information is passed in through cells.
        
        :param worksheet: Worksheet to write to
        
        :param cells: List of information to write to the worksheet
        :type cells: list
        
        :return: None
        """
        # checking to make sure there is information in all the cells about to be written.
        for cell in cells:
            if not cell:
                return False
        # Will only get here if none of the variables in this line are "False" or "None"
        for cell in cells:
            # Writing information to the current location
            worksheet.write(int(self.loc['y']), int(self.loc['x']), str(cell), cell_format)
            # Point at next cell in the row
            self.loc['x'] += 1
        # Once all the information on the line has been written, point to the beginning of the next line
        self.loc['x'] = 0
        self.loc['y'] += 1

    def addTitle(self, worksheet, title, cell_format, skip=True):
        """
        Adds a title line to the worksheet at the current marker location
        
        :param worksheet: Worksheet to add the data to
        
        :type title: str
        :param title: Title text
        
        :type skip: bool
        :param skip: Skip a line before the title line
        
        :return: None
        """
        # skip is a boolean variable to skip a line before the start of the title row.
        if skip:
            self.loc['y'] += 1
        # Merges 4 cells together and write the title at the current cursor location
        worksheet.merge_range(first_row=int(self.loc['y']),
                              first_col=int(self.loc['x']),
                              last_row=int(self.loc['y']),
                              last_col=int(self.loc['x'])+3,
                              data=str(title), cell_format=cell_format)
        self.loc['y'] += 1

# =======================================================================================
# __xlsx_....()
# Set of methods that will output different information from the burn logs.
# =======================================================================================

def __xlsx_mac__(system_list, orders, projects, save_path=""):
    try:
        for sm_order in orders:
            # Creating a file for each order with the order number in the filename.
            # TODO: Future - Add in a way to get the PO# from Trogdor and add it to the title
            if save_path == "":
                workbook = xlsxwriter.Workbook("./reports/%s_MAC_Addresses.xlsx" % sm_order)
            else:
                workbook = xlsxwriter.Workbook(save_path)
            # Setting up font styles used in the column headers
            bold = workbook.add_format({'bold': True})
            title = workbook.add_format({'bold': True, 'align': 'center'})
            for project in projects:
                # Set up a new worksheet for each project in the current order, named with the order and project numbers
                worksheet = workbook.add_worksheet('%s-%s' % (sm_order, project))
                # Creating a counter used for seeing the max number of NICs in each project. Assuming no NICs to start.
                max_nic = 0
                # Finding the max number of NICs per project
                for system in system_list:
                    if system.project == project:
                        # Determining if the system has an IPMI port
                        has_ipmi = False
                        for nic in system.network:
                            if nic.interface_type == "ipmi":
                                # If it does, set the marker to yes and break out of the "for nic..." loop
                                has_ipmi = True
                                break
                        if has_ipmi:
                            # if there are more NICs on the system ( - 1 for IPMI)
                            if len(system.network) - 1 > max_nic:
                                max_nic = len(system.network) - 1
                        else:
                            # if there is no IPMI, use this if statement instead that is not adjusted for IPMI
                            if len(system.network) > max_nic:
                                max_nic = len(system.network)
                # Writing information using the following method:
                # write ( ROW, COLUMN, CONTENT, STYLE )
                # Header Information:
                worksheet.write(0, 0, "Order # " + str(sm_order), bold)
                worksheet.merge_range(0, 1, 0, (max_nic + 1), "MAC Addresses", title)
                worksheet.write(1, 0, "Silicon Mechanics Serial", bold)
                worksheet.write(2, 0, "Project " + str(project), bold)
                worksheet.write(1, 1, "ipmi", bold)
                # Adding NIC headers, using the max number of NICs determined above
                for i in range(0, max_nic):
                    worksheet.write(1, i + 2, "eth%d" % i, bold)
                # Counter for tracking the row number:
                system_number = 1
                # Adding NIC information
                for system in system_list:
                    if system.order == sm_order and system.project == project:
                        # Write SM number on the first column of each row
                        worksheet.write(system_number + 2, 0, system.sm_number)
                        # Counter for tracking the column number:
                        nic_number = 1
                        for nic in system.network:
                            # Dedicated IPMI will always be the 2nd column for each row. Checking to see if the current
                            # nic is the dedicated IPMI. if it is not, use the next available slot and increment the
                            # counter so as to not overwrite the previous entry
                            if nic.interface_type != "ipmi":
                                worksheet.write(system_number + 2, nic_number + 1, str(nic.mac_address).upper())
                                nic_number += 1
                            else:
                                worksheet.write(system_number + 2, 1, str(nic.mac_address).upper())
                        # After all NICs in the system have been printed on the row, increment the system (row) counter
                        # and move to the next system until all systems in the project have been output.
                        system_number += 1
                        # If there are more projects, script will loop back up here.
            # Once there are no more projects in the order, close the workbook which also writes the file.
            workbook.close()
            # If there are additional orders in the list of systems that were passed in, it will loop here.
    except IOError:
        # Exception for lack of privileges in outputting the file.
        print("ERROR - Permissions not elevated to write file. Try again with Admin/Sudo access.")


def __xlsx_serial__(system_list, orders, projects, save_path=""):
    try:
        for sm_order in orders:
            # Creating a new workbook for each order passed in through system_list
            if save_path == "":
                workbook = xlsxwriter.Workbook("./reports/%s_Serial_Numbers.xlsx" % sm_order)
            else:
                workbook = xlsxwriter.Workbook(save_path)
            # Setting up styles for the current workbook
            bold = workbook.add_format({'bold': True})
            for project in projects:
                # Add a worksheet for each project named <order>-<project>
                worksheet = workbook.add_worksheet('%s-%s' % (sm_order, project))
                # Counter for determining the max number of drives for the systems in the project
                # NOTE: They should all have the same number, but this will account for any exceptions
                max_drives = 0
                # List of all drive types (by model #) that are in the project so that serials can be grouped by type
                drive_types = []
                # Getting number of drives and a list of the types of drives, by model number
                for system in system_list:
                    # Checking to make sure we are only collecting information on systems in the current project
                    if system.project == project:
                        # DRIVE COUNT MAX
                        # if the current system has more drives than the max counter...
                        if len(system.drives) > max_drives:
                            # the counter now becomes the number of drives in the current system
                            max_drives = len(system.drives)
                        # DRIVE MODEL LIST
                        # marker for if the drive model is already in the list
                        drive_model_exists = False
                        for drive in system.drives:
                            # iterating through each drive to see if it matches any drive in the list.
                            for drive_type in drive_types:
                                # If it is already in the list, set the marker to true and break out
                                # of the "for type..." loop
                                if drive.model == drive_type:
                                    drive_model_exists = True
                                    break
                            # if the marker is still set to False, the current drive model is not already in the list
                            # and needs to be added.
                            if not drive_model_exists:
                                drive_types.append(drive.model)
                # Header information for content that exists on all systems
                worksheet.write(0, 0, "SM Number", bold)
                worksheet.write(0, 1, "Motherboard Model", bold)
                worksheet.write(0, 2, "Motherboard Serial", bold)
                worksheet.write(0, 3, "Supermicro Serial", bold)
                # setting up a counter to track current row number
                system_number = 1
                for system in system_list:
                    # setting up a counter to track current column number
                    drive_number = 1
                    extra = 0
                    # Making sure the system being output is in the current project
                    if system.project == project:
                        # outputting the basic system information (sm#, mobo type, mobo serial)
                        # in their respective columns
                        worksheet.write(system_number, 0, system.sm_number)
                        worksheet.write(system_number, 1, system.fru_information.motherboard_part_number)
                        worksheet.write(system_number, 2, system.fru_information.motherboard_serial)
                        worksheet.write(system_number, 3, system.dmi_information.chassis_serial)
                        # iterating through the list of drive types.
                        for drive_type in drive_types:
                            # writing a leading model number column that succeeding serial number columns will refer to
                            worksheet.write(0, 3 + drive_number + extra, "Drive Model", bold)
                            worksheet.write(system_number, 3 + drive_number + extra, drive_type)
                            for drive in system.drives:
                                if drive.model == drive_type:
                                    worksheet.write(0, 4 + drive_number + extra, "Drive %d" % drive_number, bold)
                                    worksheet.write(system_number, 4 + drive_number + extra, drive.serial)
                                    drive_number += 1
                            extra += 1

                        system_number += 1
            workbook.close()
    except Exception as e:
        print(str(e))


def __xlsx_sys_report__(system_list, orders, projects, save_path=""):
    """
    This method uses the Excel Writer class in conjunction with XLSX writer library to create a complete system
    report, output as an xlsx format.
    
    :param system_list: list of systems downloaded from the rethink db on Jarvis
    :type system_list: dict
    
    :param orders: list of orders that are in the system list
    :type orders: list
    
    :param projects: list of projects that are in the system list
    :type projects: list
    
    :param save_path: location to save the file. Default is ./reports/
    :type save_path: str
    
    :return: None
    """
    try:
        for sm_order in orders:
            # Creating a new workbook for each order passed in through system_list
            if save_path == "":
                workbook = xlsxwriter.Workbook("./reports/%s_System_Report.xlsx" % sm_order)
            else:
                workbook = xlsxwriter.Workbook(save_path)
            # Setting up styles for the current workbook
            title_format = workbook.add_format({'bold': True,
                                                'font_name': 'Consolas',
                                                'font_size': 10,
                                                'font_color': 'white',
                                                'valign': 'Center',
                                                'bg_color': '#595959'})
            body_format = workbook.add_format({'bold': False,
                                               'font_name': 'Consolas',
                                               'font_size': 10})
            for system in system_list:
                # Create an instance of ExcelWriter that will track current location on the worksheet
                writer = ExcelWriter()
                # Add a new sheet to the workbook with the name of the sheet corresponding to the system SM number
                worksheet = workbook.add_worksheet(system.sm_number)
                # Setting Header
                worksheet.set_margins(top=1.1, right=0.5, left=0.5)
                worksheet.set_header('&L&G', {'image_left':'header_xlsx.png'})
                # Setting the width of the columns used
                worksheet.set_column('A:A', width=20)
                worksheet.set_column('B:B', width=38.6)
                worksheet.set_column('C:D', width=17)
                # Writing System Information
                # ------------------------------------
                # General System Information
                # ------------------------------------
                writer.addTitle(worksheet,
                                "General System Information",
                                skip=False,
                                cell_format=title_format)
                writer.addLine(worksheet,
                               [
                                   "Order Number",
                                   str(system.order)
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Project Number",
                                   str(system.project)
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "SM Number",
                                   str(system.sm_number)
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Platform",
                                   str(system.dmi_information.system_platform)
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Operating System",
                                   str(system.os)
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Kernel Version",
                                   str(system.os_kernel)
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Motherboard/BIOS",
                                   "{mb} / {ver} ({date})".format(mb=system.fru_information.motherboard_part_number,
                                                                  ver=system.bios.version,
                                                                  date=system.bios.version_date)
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   'Motherboard Serial',
                                   system.fru_information.motherboard_serial
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   'SUM Activated',
                                   system.sum_activated
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   'IPMI Firmware',
                                   system.ipmi_firmware
                               ], body_format)
                # ------------------------------------
                # Networking Information
                # ------------------------------------
                writer.addTitle(worksheet, "Networking", title_format)
                for nic in system.network:
                    if nic.interface_type == "ipmi":
                        writer.addLine(worksheet,
                                       [
                                           'IPMI',
                                           'ON-BOARD IPMI',
                                           str(nic.mac_address).upper()
                                       ],
                                       body_format)
                    else:
                        writer.addLine(worksheet,
                                       [
                                           str(nic.name).upper(),
                                           nic.model,
                                           str(nic.mac_address).upper()
                                       ],
                                       body_format)
                # ------------------------------------
                # Hardware Information
                # ------------------------------------
                writer.addTitle(worksheet, "Hardware", title_format)
                for cpu in system.cpu:
                    writer.addLine(worksheet, [str(cpu.socket).upper(), cpu.model], cell_format=body_format)
                for ram in system.ram:
                    writer.addLine(worksheet,
                                   [
                                       str(ram.slot).upper(),
                                       "{rtype} {rspeed} {rsize}".format(rtype=ram.type,
                                                                         rspeed=ram.speed,
                                                                         rsize=ram.size),
                                       ram.manufacturer,
                                       ram.model
                                   ], body_format)
                drive_count = 0
                for drive in system.drives:
                    writer.addLine(worksheet,
                                   [
                                       "Drive{d_count}".format(d_count=drive_count),
                                       drive.model,
                                       drive.capacity
                                   ], body_format)
                    drive_count += 1
                del drive_count
                for slot in system.card_slots:
                    if slot.used:
                        writer.addLine(worksheet,
                                       [
                                           "Populated",
                                           slot.designation,
                                           slot.card
                                       ], body_format)
                    else:
                        writer.addLine(worksheet, ["Available", slot.designation], body_format)
                # ------------------------------------
                # DMI Information
                # ------------------------------------
                writer.addTitle(worksheet, "DMI", title_format)
                writer.addLine(worksheet,
                               [
                                   "UUID",
                                   system.dmi_information.uuid
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Chassis Serial",
                                   system.dmi_information.chassis_serial
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "SM Number",
                                   system.dmi_information.sm_number
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Superserver Model",
                                   system.dmi_information.superserver_model
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "System Platform",
                                   system.dmi_information.system_platform
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "System Manufacturer",
                                   system.dmi_information.system_manufacturer
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Superserver Serial",
                                   system.dmi_information.superserver_serial
                               ], body_format)
                # ------------------------------------
                # FRU Information
                # ------------------------------------
                writer.addTitle(worksheet, "FRU", title_format)
                writer.addLine(worksheet,
                               [
                                   "Superserver Serial",
                                   system.fru_information.superserver_serial
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Chassis Serial",
                                   system.fru_information.chassis_serial
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Board Part Number",
                                   system.fru_information.motherboard_part_number
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Product Manufacturer",
                                   system.fru_information.product_manufacturer
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Board Manufacturer",
                                   system.fru_information.motherboard_manufacturer
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Product Serial Number",
                                   system.fru_information.product_serial_number
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Product Asset Tag",
                                   system.fru_information.product_asset_tag
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Product Model Number",
                                   system.fru_information.product_model_number
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Product Name",
                                   system.fru_information.product_name
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Superserver Model",
                                   system.fru_information.superserver_model
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Board Serial",
                                   system.fru_information.motherboard_serial
                               ], body_format)
                writer.addLine(worksheet,
                               [
                                   "Product Version",
                                   system.fru_information.product_version
                               ], body_format)
                # ------------------------------------
                # BIOS Settings
                # ------------------------------------
                if system.sum_activated:
                    writer.addTitle(worksheet, "BIOS Settings", title_format)
                    writer.addLine(worksheet,
                                   [
                                       "Boot Mode",
                                       system.bios.boot_mode
                                   ], body_format)
                    boot_count = 0
                    for device in system.bios.boot_order:
                        writer.addLine(worksheet,
                                       [
                                           "Boot Device {0}".format(boot_count),
                                           device
                                       ], body_format)
                        boot_count += 1
                    del boot_count
                    writer.addLine(worksheet,
                                   [
                                       "SATA Controller Mode",
                                       system.bios.sata_controller_mode
                                   ], body_format)
                    writer.addLine(worksheet,
                                   [
                                       "Wait for F1 on Boot",
                                       system.bios.f1_wait
                                   ], body_format)
                    writer.addLine(worksheet,
                                   [
                                       "Instant Off",
                                       system.bios.instant_off
                                   ], body_format)
                # ExcelWriter only tracks location for a single sheet. All information has been written so the writer
                # needs to be deleted so it's not used more than once.
                print("Worksheet complete for {sm}.\n".format(sm=system.sm_number))
                del writer
            # Once all sheets have been written, close the workbook.
            print("All systems processed for {order_number}.\n".format(order_number=system_list[0].order))
            workbook.close()
    # General exception catch -- usually used when the user does not have write permissions to the directory.
    except Exception as e:
        print(str(e))

# =======================================================================================
# OutputLine & OutputCategory -
# Output classes are for ease of outputting into PDF with formatting. See for-loop below.
# =======================================================================================

class OutputLine:
    def __init__(self, sections_per_row=2):
        self.line_content = []
        self.sections = sections_per_row

    @staticmethod
    # def __check_overrun__(string, size):
    #     """
    #
    #     :param string:
    #     :param size:
    #     :return:
    #     """
    #     if isinstance(string, basestring) and isinstance(sections, int):
    #         length_key = [100, 50, 25]
    #         if 0 < sections < 4:
    #             if len(string) > length_key[sections-1]:
    #                 return string[:length_key[sections-1]]
    #             else:
    #                 return string
    #         else:
    #             return False
    #     else:
    #         raise TypeError

    def add_row(self, content):
        """
        :param content:
        :return:
        """
        if isinstance(content, list):
            if len(content) == 2:
                self.line_content.append([1, 35, str(self.__check_overrun__(content[0], len(content)))])
                self.line_content.append([2, 100, str(self.__check_overrun__(content[1], len(content)))])
            elif len(content) < 4 and len(content) != 2:
                length_key = [135, 0, 45, 33]
                try:
                    for i in range(0, len(content)):
                        self.line_content.append([i + 1,
                                                  length_key[i + 1],
                                                  str(self.__check_overrun__(content[i], len(content)))])
                except IndexError:
                    return False
        elif isinstance(content, str):
            self.line_content.append([1, 35, ""])
            self.line_content.append([2, 100, content])
        else:
            raise TypeError

            # def add_section(self, location, content):
            #     """
            #
            #     :param location:
            #     :param content:
            #     :return:
            #     """
            #     if isinstance(content, basestring)


class OutputCategory:
    def __init__(self, head=""):
        """
        :param head: Header/Title for the section
        """
        self.category_header = head
        # lines contains the content, stored in OutputLine
        self.lines = []


# The following class was taken and adapted from an example at https://github.com/reingart/pyfpdf/tree/master/tutorial
class PDF(FPDF):
    def __init__(self):
        FPDF.__init__(self, unit='in', format='letter')

    # def header(self):
    #     # Logo
    #     #self.image('header.png', 0, 0, 8.5)
    #     # Line break
    #     self.ln(0.59)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-0.45)
        # Arial italic 8
        self.set_font('Arial', size=8)
        # Page number
        self.cell(0, 0.2, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')


def save_to_pdf(system_list, order_number, filename="output", customer=""):
    """
    save_to_pdf will take a system list (Server() in SystemHW.py) and generate a full system report with output to
    the filename designated in the argument list. Default filename is output.pdf. Always outputs in the same
    directory as the program.
    :param system_list: list of Server()
    :param order_number: Order number used for cover sheet
    :param filename: string used for "____" in  "____.pdf"
    :param customer: Customer name used for cover sheet
    :return: Returns true or false depending on success or failure
    :rtype Boolean
    """
    # return false if list is empty
    if not system_list:
        return False
    # create a new pdf object
    pdf = PDF()
    pdf.alias_nb_pages()
    pdf.set_font('Times', '', 12)
    # generating pre-set sizes for different sections. made for ease of change later.
    head1 = 16
    head2 = 12
    body = 10
    # Adding cover page
    pdf.add_page()

    pdf.set_font('Arial', 'B', 24)
    pdf.cell(0, 5.15, "", 0, 3)
    pdf.cell(0, 0.7, "Silicon Mechanics System Report", 0, 3, 'C')
    pdf.set_font('Arial', '', head1)
    # Customer name comes in from the rethinkdb with underlines instead of spaces. Replacing underlines with spaces:
    cust_list = list(customer)
    for i in range(0, len(cust_list)):
        if cust_list[i] == "_":
            cust_list[i] = " "
    customer = "".join(cust_list)
    pdf.cell(0, 0.4, customer, 0, 1, 'C')
    pdf.set_font('Arial', 'I', body)
    if order_number:
        if 'A' >= str(order_number[0]).upper() >= 'Z':
            pdf.cell(0, 0.35, "Order %s" % order_number, 0, 1, 'C')
        else:
            pdf.cell(0, 0.35, "Project %s" % order_number, 0, 1, 'C')
    else:
        return False
    pdf.cell(0, 0.35, "Report Generated %s" % str(date.today()), 0, 1, 'C')
    # Loading systems into OutputCategory and OutputLine
    for server in system_list:
        print('{:.<75}'.format(str("Generating report for " + server.sm_number)), end="")
        pdf.add_page()
        pdf.image('header.png', 0, 0, 8.5)
        pdf.set_font('Arial', '', head1)
        content = []
        # -----------------------------Page Header-----------------------------
        pdf.cell(0, 0.39, "SM Number " + str(server.sm_number), 0, 1)
        # -----------------------------CPU-----------------------------
        # Output Example:
        # CPU and RAM
        # CPU0          <manufacturer> <model> @ <speed> ("cpu.model" contains all this information)
        # CPU1          <manufacturer> <model> @ <speed>
        # P1-DIMMA1     <manufacturer> <model> - <size>
        # P1-DIMMB1     <manufacturer> <model> - <size>
        cpu_info = OutputCategory("CPU and RAM")
        i = 0
        for cpu in server.cpu:
            new_line = OutputLine()
            new_line.add_row("CPU%d" % i, cpu.model)
            cpu_info.lines.append(new_line)
            i += 1
        del i
        for ram in server.ram:
            new_line = OutputLine()
            new_line.add_row(ram.slot, "%s %s - %s" % (ram.manufacturer, ram.model, ram.size))
            cpu_info.lines.append(new_line)
        content.append(cpu_info)
        # -----------------------------Motherboard Information-----------------------------
        # Output Sample:
        # Motherboard Information
        # Model             <motherboard_part_number>
        # Serial            <motherboard_serial>
        # Manufacturer      <motherboard_manufacturer>
        mobo = {"Model: ": server.fru_information.motherboard_part_number,
                "Serial: ": server.fru_information.motherboard_serial,
                "Manufacturer: ": server.fru_information.motherboard_manufacturer}
        mobo_info = OutputCategory("Motherboard Information")
        for key in mobo:
            new_line = OutputLine()
            new_line.add_row(str(key), str(mobo[key]))
            mobo_info.lines.append(new_line)
        content.append(mobo_info)
        del mobo
        # -----------------------------DMI Information-----------------------------
        # Output Sample:
        # DMI Information
        # Chassis Serial        <chassis_serial>
        # Superserver Serial    <superserver_serial>
        # Manufacturer          <system_manufacturer>
        # Platform              <system_platform>
        dmi_info = OutputCategory("DMI Information")
        dmi = {"Chassis Serial: ": server.dmi_information.chassis_serial,
               "Superserver Serial: ": server.dmi_information.superserver_serial,
               "Manufacturer: ": server.dmi_information.system_manufacturer,
               "Platform: ": server.dmi_information.system_platform}
        for key in dmi:
            new_line = OutputLine()
            new_line.add_row(str(key), str(dmi[key]))
            dmi_info.lines.append(new_line)
        content.append(dmi_info)
        del dmi
        # -----------------------------IPMI Information-----------------------------
        # Output Sample:
        # IPMI Information
        # MAC:              <mac_address>
        # IPMI Firmware     <ipmi_firmware>
        ipmi_info = OutputCategory("IPMI Information")
        for nic in server.network:
            if nic.interface_type == "ipmi":
                ipmi_nic = OutputLine()
                ipmi_nic.add_row("MAC: ", nic.mac_address)
                ipmi_info.lines.append(ipmi_nic)
                break
        ipmi = OutputLine()
        ipmi.add_row("IPMI Firmware: ", server.ipmi_firmware)
        ipmi_info.lines.append(ipmi)
        content.append(ipmi_info)
        # -----------------------------Expansion Slot Information-----------------------------
        # Output Sample:
        # Expansion Slot Information
        # Available         <designation>
        # Populated         <designation> - <card>
        exp_slot = OutputCategory("Expansion Slot Information")
        for slot in server.card_slots:
            if slot.used:
                expansion = OutputLine()
                expansion.add_row("Populated", "%s - %s" % (slot.designation, slot.card))
                exp_slot.lines.append(expansion)
            else:
                expansion = OutputLine()
                expansion.add_row("Available", slot.designation)
                exp_slot.lines.append(expansion)
        content.append(exp_slot)
        # -----------------------------Drive Information-----------------------------
        # Output Sample:
        # Drive Information
        # SATA BIOS Mode:   AHCI
        # Slot 1:           <brand>
        #                   Capacity: <capacity>
        #                   Model: <model>
        #                   Serial: <serial>
        hdd_info = OutputCategory("Drive Information")
        if server.sum_activated:
            hdd = OutputLine()
            hdd.add_row("SATA BIOS Mode:", str(server.bios.sata_controller_mode))
            hdd_info.lines.append(hdd)
        for drive in server.drives:
            drive_info = {"Slot: %s" % drive.slot: drive.brand, "__cap__": "Capacity: %s" % drive.capacity,
                          "__mod__": "Model: %s" % drive.model, "__ser__": "Serial: %s" % drive.serial}
            for key in drive_info:
                hdd = OutputLine()
                if "__" in key:
                    hdd.add_row("", drive_info[key])
                else:
                    hdd.add_row(str(key), str(drive_info[key]))
        content.append(hdd_info)
        # -----------------------------NIC Information-----------------------------
        # Output Sample:
        # Network Information
        # eno1              <interface_type> - <mac_address>
        # eno2              <interface_type> - <mac_address>
        nic_info = OutputCategory("Network Information")
        for nic in server.network:
            if nic.interface_type is not "ipmi":
                other_nic = OutputLine()
                other_nic.add_row(nic.name, "%s - %s" % (nic.interface_type, nic.mac_address.upper()))
                nic_info.lines.append(other_nic)
        content.append(nic_info)
        # -----------------------------SUM Information-----------------------------
        # Output Sample:
        #
        #
        #
        bios_info = OutputCategory("BIOS Settings and Information")
        if not server.sum_activated:
            SUM = OutputLine()
            SUM.add_row("SUM Not activated, BIOS Information not available.", "")
            bios_info.lines.append(SUM)
        else:
            sum_list = {}
            count = 1
            for value in server.bios.boot_order:
                sum_list["Boot Volume %s:" % count] = value
                count += 1
            sum_list["SUM Activated:"] = "True"
            sum_list["Boot Mode:"] = str(server.bios.boot_mode)
            sum_list["Wait for F1 If Error:"] = server.bios.f1_wait
            sum_list["Power Button Action:"] = server.bios.instant_off
            for key in sorted(sum_list):
                SUM = OutputLine()
                SUM.add_row(str(key), str(sum_list[key]))
                bios_info.lines.append(SUM)
        content.append(bios_info)
        # END LOADING INFORMATION INTO Output CLASSES
        # Adding information from Output classes into pdf class with formatting
        try:
            for category in content:
                if len(category.lines) != 0:
                    pdf.set_font('Arial', 'B', head2)
                    pdf.cell(0, 0.31, category.category_header, 0, 1)
                    pdf.set_font('Arial', '', body)
                    for line in category.lines:
                        if len(line.line_content[0]) != 0:
                            pdf.cell(35, 5, line.line_title, 0, 0)
                            line_count = 1
                            for subline in line.line_content:
                                if line_count > 1:
                                    pdf.cell(1.4, 0.19, "", 0, 0)
                                pdf.multi_cell(5.1, 0.19, str(subline), 0, 1)
                                line_count += 1
        except IndexError:
            print("**Index Issue**")
            exit()
        print('[ OK ]')
    pdf.output("%s.pdf" % filename, 'F')
    return True


def save_to_csv(system_list, filename="output"):
    """
    Outputs a csv file with excel encoding that lists major serial and model information for each system in a set of
    systems such as an order or project
    :param system_list: list of Server() (class / structure from SystemHW.py) to include in the file
    :param filename: filename used -> ____.csv (default is output.csv if string is not passed to method
    :return: Returns true or false depending on success or failure
    :rtype Boolean
    """
    # Making sure there is content in system_list
    if not system_list:
        return False
    # Max Drives and NIC will ensure enough columns are created for the CSV
    max_drives = 0
    max_nic = 0
    # Check all systems for max drives and NICs
    for server in system_list:
        drive_counter = len(server.drives)
        if drive_counter > max_drives:
            max_drives = drive_counter
        nic_counter = len(server.network)
        if nic_counter > max_nic:
            max_nic = nic_counter
    # Setup complete. Starting write of information
    with open('%s.csv' % filename, 'w') as csvfile:
        # Setting up header information
        fieldnames = ["SM_SERIAL", "MB_SERIAL", "CHASSIS_SERIAL", "IPMI"]
        # Adding enough columns for all NICs
        for i in range(0, max_nic - 1):
            print("Max NICs: " + str(max_nic))
            fieldnames.append("NIC_" + str(i) + "_TYPE")
            fieldnames.append("NIC_" + str(i))
        # Adding enough columns for all drives
        for i in range(0, max_drives):
            fieldnames.append("DRIVE_" + str(i) + "_MODEL")
            fieldnames.append("DRIVE_" + str(i) + "_SERIAL")
        # Applying header
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        # End Header
        # Start system information
        for server in system_list:
            print("Information for %s:" % server.sm_number)
            print(" - MB Serial:" + str(server.fru_information.motherboard_serial))
            print(" - Chassis Serial:" + str(server.dmi_information.chassis_serial))
            # Set Basics
            row = {'SM_SERIAL': server.sm_number,
                   'MB_SERIAL': server.fru_information.motherboard_serial,
                   'CHASSIS_SERIAL': server.dmi_information.chassis_serial}
            # Finding IPMI in list of network adapters
            i = 0
            for nic in server.network:
                if nic.interface_type == "ipmi":
                    row['IPMI'] = nic.mac_address
                    print(" - IPMI MAC:" + str(nic.mac_address))
                else:
                    row['NIC_' + str(i)] = nic.mac_address
                    row['NIC_' + str(i) + '_TYPE'] = nic.interface_type
                    print(" - NIC_%d:" % i + str(nic.mac_address))
                    i += 1
            i = 0
            for drive in server.drives:
                if not drive.virtual:
                    print(" - Drive number: " + str(i))
                    row['DRIVE_' + str(i) + '_MODEL'] = drive.model
                    row['DRIVE_' + str(i) + '_SERIAL'] = drive.serial
                    print("   - Serial: %s\n   - Model:%s" % (drive.serial, drive.model))
                    i += 1
            writer.writerow(row)
    return True


def save_to_xlsx(system_list, output_path="", MAC=True, Serial=True, SysReport=True):
    # The following will gather a list of all projects and orders passed in by system list. I realize that there should
    # only be one order at a time max, but I wanted to build this in for future expansion so that even if multiple
    # orders are passed through system_list that it will be able to handle it.

    projects = []
    orders = []
    # Setting up orders and projects list for all systems passed in
    for system in system_list:
        # First run to set up the first entry in each list:
        if not projects and not orders:
            projects.append(system.project)
            orders.append(system.order)
        else:
            # Setting up markers and assuming the project / order does not already exist in the list.
            project_exists = False
            order_exists = False
            # Iterate through the list of orders and projects to see if the order or project already exists
            for sm_order in orders:
                if system.order == sm_order:
                    # if it does exist, set the marker to true and break out of the "for sm_order..." loop.
                    order_exists = True
                    break
            # Same iteration through project list
            for project in projects:
                if system.project == project:
                    project_exists = True
                    break
            # Based on the results / markers, add the order / project to the corresponding list if the marker is still
            # set to False.
            if not order_exists:
                orders.append(system.order)
            if not project_exists:
                projects.append(system.project)
    # ---------------------------------------------------
    # Starting output
    # ---------------------------------------------------
    if MAC:
        __xlsx_mac__(system_list, orders, projects)
    if Serial:
        __xlsx_serial__(system_list, orders, projects)
    if SysReport:
        __xlsx_sys_report__(system_list, orders, projects)