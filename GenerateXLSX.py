#! /usr/bin/env python

import RethinkDBQuery
import FileOut

loop = False

while not loop:
    pos_selection = raw_input("Pull information from project, order, or serial? (p/o/s): ")
    if pos_selection.lower() == 'p' or pos_selection.lower() == 'o' or pos_selection.lower() == 's':
        loop = True
        project = ""
        order = ""
        # Checking if program should run project or order report
        if pos_selection.lower() == 'p':
            project = raw_input("Enter Project Number: ")
            systems = RethinkDBQuery.run_query(project=project)
        elif pos_selection.lower() == 's':
            sm_number = raw_input("Enter SM Number: ")
            systems = RethinkDBQuery.run_query(sm_number=sm_number)
        else:
            order = raw_input("Enter Order Number: ")
            systems = RethinkDBQuery.run_query(order=order)
        success = FileOut.save_to_xlsx(system_list=systems)
        if str(raw_input("Run again? (y/n)")).lower() == "y":
            loop = False
    else:
        "Input not valid, try again. Please note - 'project' will not work in place of 'p' enter only a letter and" \
        "not the whole word"
