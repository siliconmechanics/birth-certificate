# README #

## What is this repository for? ##

* Quick summary
* Version 0.2.0

## How do I get set up? ##

### Dependencies ###
* python 2.7
* Python Packages (pip install): fpdf, rethinkdb, fuzzywuzzy

## Run Instructions: ##
Ensure write access is available for current directory. Files are output to the same directory as the python files.

For the System PDF Report, run: python PullReport.py

For the MAC and Serials CSV, run: python PullMACandSerials.py

During each program, follow the CLI prompts.

## Who do I talk to? ##

Owner: Joe Walker

joe.walker@siliconmechanics.com

425-420-1288