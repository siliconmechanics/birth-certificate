from __future__ import print_function
from fuzzywuzzy import fuzz
import rethinkdb as r
import SystemHW
import re


def __decode_boot__(value, method):
    if method == "UEFI" and 0 <= int(value, 16) < 9:
        uefi_key = ["Hard Disk", "CD / DVD", "USB Hard Disk", "USB CD / DVD", "USB Key", "USB Floppy", "Network",
                    "UEFI AP", "Disabled"]
        return uefi_key[int(value, 16)]
    elif method == "Legacy" and 0 <= int(value, 16) < 8:
        legacy_key = ["Hard Disk", "CD / DVD", "USB Hard Disk", "USB CD / DVD", "USB Key", "USB Floppy", "Network",
                      "Disabled"]
        return legacy_key[int(value, 16)]
    elif method == "Dual" and 0 <= int(value, 16) < 15:
        dual_key = ["UEFI Hard Disk", "UEFI CD / DVD", "UEFI USB Hard Disk", "UEFI USB CD / DVD", "UEFI USB Key",
                    "UEFI USB Floppy", "UEFI Network", "UEFI AP", "Hard Disk", "CD / DVD", "USB Hard Disk",
                    "USB CD / DVD", "USB Key", "USB Floppy", "Network", "Disabled"]
        return dual_key[int(value, 16)]


# def __check_for_result__(result, name, sub1="", sub2="", sub3="", return_boolean=False):
def __check_for_result__(result, key_list, return_bool=False):
    """
    internal method to test if a set of results was returned or if the result is blank. Adds print output for
    failure or success.
    """
    # If the key at the front of the list is in the current dictionary
    if key_list[0] in result:
        # And if there is more in the list than that one key,
        if len(key_list) > 1:
            # Call the method again but take out the first entry in the list. Return used for recursion.
            return __check_for_result__(result[key_list[0]], key_list[1:], return_bool=return_bool)
        else:
            # If it is the last element in the list, the value has been found.
            if return_bool:
                # Boolean return
                return True
            # Value return
            return result[key_list[0]]
    # The element was not found. Return False
    else:
        return False


def __prompt_mismatch__(option1, option2):
    """
    Prompt the user to select which string to use out of option1 and option2
    :param option1:
    :param option2:
    :return:
    """
    # Infinite loop until one value is selected
    while True:
        value = raw_input("Trogdor and Jarvis do not match. Which one would you like to use:\n1)%s\n2)%s\n=>" %
                          (option1, option2))
        if str(value) == "1":
            return option1
        if str(value) == "2":
            return option2


def __check_match__(string1, string2):
    # Returns blank string if both are None or blank or "Default"
    if (not string1 and not string2) or (string1 == "Default" and string2 == "Default"):
        return ""
    # Checking to ensure both strings have valid information to compare to.
    # Currently checks for None, ""(blank) and "None"
    if not string1 or string1 == "None" or "Default":
        return string2
    if not string2 or string2 == "None":
        return string1
    # Checking to see if strings exactly match
    if string1 == string2:
        return string1
    # Using fuzzy logic comparison. If the matching ratio is under 75% matched, ask the user.
    if fuzz.ratio(string1, string2) < 75:
        return __prompt_mismatch__(string1, string2)
    # If there is information in both strings, there isn't an exact match, and they have a match ratio of greater
    # than 75%, use the string that is longer.
    if len(string1) > len(string2):
        return string2
    return string1


def __dmi__(dmi_info):
    """
    Desktop Management Interface

    Path Used:
    System -> Components -> Motherboard -> DMI/Dmi

    Variables Used:               Expected (Example):
        - Asset Tag                 ""
        - Chassis Serial            ""
        - SM Number                 "SM112416"
        - Superserver Model Number  ""
        - Superserver Serial Number ""
        - System Manufacturer       "Silicon Mechanics"
        - System Platform           "Rackform_R101.v6"
        - UUID                      "F9FE72F0-F61F-11E6-8058-7ACFC4AA3802"
    """
    new_dmi = SystemHW.DMI()
    # UUID
    new_dmi.uuid = __check_for_result__(dmi_info, ['UUID'])
    # Superserver Model Number
    new_dmi.superserver_model = __check_for_result__(dmi_info, ['Superserver Model Number'])
    # System Platform
    new_dmi.system_platform = __check_for_result__(dmi_info, ['System Platform'])
    # System Manufacturer
    new_dmi.system_manufacturer = __check_for_result__(dmi_info, ['System Manufacturer'])
    # Superserver Serial Number
    new_dmi.superserver_serial = __check_for_result__(dmi_info, ['Superserver Serial Number'])
    # Chassis Serial
    new_dmi.chassis_serial = __check_for_result__(dmi_info, ['Chassis Serial'])
    # SM Number
    new_dmi.sm_number = __check_for_result__(dmi_info, ["SM Number"])
    return new_dmi


def __fru__(fru_info):
    """
    Field Replaceable Unit

    Path Used:
      System -> Components -> IPMI -> FRU / Fru
    Variables Used:               Expected (Example):
       - Board Manufacturer
       - Board Manufacturing Date
       - Board Part Number
       - Board Serial Number
       -
       -
       -
       -

    :param fru_info: FRU information from the jarvis query
    :type fru_info: dict

    :return:
    """

    new_fru = SystemHW.FRU()
    # Motherboard Serial Number
    new_fru.motherboard_serial = __check_for_result__(fru_info, ['Board Serial Number'])
    # Motherboard Part Number
    new_fru.product_name = __check_for_result__(fru_info, ['Product Name'])
    # Superserver Serial Number
    new_fru.superserver_serial = __check_for_result__(fru_info, ['Superserver Serial'])
    # Motherboard Part Number
    new_fru.motherboard_part_number = __check_for_result__(fru_info, ['Board Part Number'])
    # Product (system) Model Number
    new_fru.product_model_number = __check_for_result__(fru_info, ['Product Model Number'])
    # Superserver Model Number
    new_fru.superserver_model = __check_for_result__(fru_info, ['Superserver Model'])
    # Chassis Serial
    new_fru.chassis_serial = __check_for_result__(fru_info, ['Chassis Serial'])
    # Motherboard Manufacturer
    new_fru.motherboard_manufacturer = __check_for_result__(fru_info, ['Board Manufacturer'])
    # Product Manufacturer
    new_fru.product_manufacturer = __check_for_result__(fru_info, ['Product Manufacturer'])
    # Product Version
    new_fru.product_version = __check_for_result__(fru_info, ['Product Version'])
    # Product Serial Number
    new_fru.product_serial_number = __check_for_result__(fru_info, ['Product Serial Number'])
    # Product Asset Tag
    new_fru.product_asset_tag = __check_for_result__(fru_info, ['Product Asset Tag'])
    # Board Manufactured Date
    new_fru.motherboard_manufacturing_date = __check_for_result__(fru_info, ['Board Manufacturing Date'])
    return new_fru


def __ipmi__(ipmi_info, server_object):
    """
    Intelligent Platform Management Interface

    Path Used:
        System -> Components -> IPMI

    Variables Used:                   Expected (Example):
       - MAC/Mac                       "0C:C4:7A:95:83:32"
       - IPMI Firmware Rev/Firmware    "1.11"
       - Ip                            "0.0.0.0"

    :param ipmi_info: 
    :type ipmi_info: dict

    :param server_object: Server object to write information to
    :type server_object: Server

    :return: 
    """

    # IPMI MAC Address
    ipmi_mac = __check_for_result__(ipmi_info, ['MAC'])
    if not ipmi_mac:
        ipmi_mac = __check_for_result__(ipmi_info, ['Mac'])
    ipmi_nic = SystemHW.Nic(mac=str(ipmi_mac),
                            nic_type="ipmi",
                            ip=__check_for_result__(ipmi_info, ["Ip"]))
    server_object.network.append(ipmi_nic)

    # IPMI Firmware Version
    ipmi_fw = __check_for_result__(ipmi_info, ['IPMI Firmware Rev'])
    if not ipmi_fw:
        ipmi_fw = __check_for_result__(ipmi_info, ['Firmware'])
    server_object.ipmi_firmware = str(ipmi_fw)


def __nic__(nic_list):
    """
    Network Interface Cards

    Path Used:
      System -> Components -> Network Adapters

    Variables Used:               Expected (Example):
      - MAC/Mac                   "0c:c4:7a:95:88:5c"
      - Name                      "eno1"
      - Model                     "I210 Gigabit"
      - Onboard                   true

    :param nic_list: 
    :return: 
    """
    nic_objects = []
    for nic in nic_list:
        mac_address = __check_for_result__(nic, ['Mac'])
        if not mac_address:
            mac_address = __check_for_result__(nic, ['MAC'])
        new_nic = SystemHW.Nic(mac=mac_address,
                               nic_name=nic['Name'],
                               nic_model=nic['Model'])
        if nic['Onboard']:
            new_nic.interface_type = "onboard"
        else:
            new_nic.interface_type = "add-on"
        nic_objects.append(new_nic)
    return nic_objects


def __drives__(drive_list):
    """
    SSDs / NVMe / Disk Drives

    Path Used:
      System -> Components -> Drives

    Variables Used:               Expected (Example):
      - Manufacturer              "Intel"
      - Model                     "SDLF1CRR-019T-1HA2"
      - Capacity                  "1.745 TB"
      - Serial                    "A00819D2"
      - Slot                      "0"
      - Controller Address        "/c0/e252/s0"
      - Controller                "LSI 0"
      - Virtual                   False

    :param drive_list: 
    :return: 
    """
    drive_objects = []
    for drive in drive_list:
        cont_address = __check_for_result__(drive, 'Controller Address')
        if not cont_address:
            cont_address = __check_for_result__(drive, 'Controller address')
        if not drive['Virtual']:
            drive_objects.append(SystemHW.Drive(brand=drive['Manufacturer'],
                                                model=drive['Model'],
                                                serial=drive['Serial'],
                                                size=drive['Capacity'],
                                                slot=drive['Slot'],
                                                path=cont_address,
                                                controller=drive['Controller']))
    return drive_objects


def __ram__(ram_list):
    """
    RAM Sticks

    Path Used:
      System -> Components -> RAM

    Variables Used:               Expected (Example):
      - Slot                      "P1-DIMMA1"
      - Serial                    "1353D115"
      - Size                      "8192 MB"
      - Model                     "9ASF1G72PZ-2G3B1"
      - Manufacturer              "Micron"
      - Type                      "DDR4"

    :param ram_list: 
    :return: 
    """
    ram_objects = []

    for stick in ram_list:
        ram_objects.append(SystemHW.RAM(slot_no=stick['Slot'],
                                        serial=stick['Serial'],
                                        size=stick['Size'],
                                        model=stick['Model'],
                                        manufacturer=stick['Manufacturer'],
                                        connection_type=stick['Type'],
                                        speed=stick['Speed']))

    return ram_objects


def __cpu__(cpu_list):
    """
    CPUs

    Path Used:
      System -> Components -> CPUs

    Variables Used:               Expected (Example):
      - Socket Number             "Cpu1"
      - Current Speed             "2100 MHz"
      - Cores                     "8"
      - Model                     "Intel Xeon CPU E5-2620 v4 @ 2.10GHz"

    :param cpu_list: 
    :return: 
    """
    cpu_objects = []
    for processor in cpu_list:
        speed = __check_for_result__(processor, ['Current Speed'])
        if not speed:
            speed = __check_for_result__(processor, ['Current speed'])
        socket = __check_for_result__(processor, ['Socket Number'])
        if not socket:
            socket = __check_for_result__(processor, ['Socket number'])
        cpu_objects.append(
            SystemHW.CPU(model=processor['Model'],
                         cores=int(processor['Cores']),
                         speed=speed,
                         socket=socket))
    return cpu_objects


def __pci__(slot_list):
    """
    PCIe Devices

    Path Used:
      System -> Components -> PCI Slots

    Variables Used:               Expected (Example):
      - Usage                     "Available"
      - Designation               "CPU2 SLOT2 PCI-E 3.0 X8"
      - Type                      "x8 PCI Express 3 x8"
      - Card                      "AVAGO 3108 MegaRAID (Onboard 0, controller LSI 0)" - After RE

    :param slot_list: 
    :return: 
    """
    slot_objects = []

    for slot in slot_list:
        card = None
        if "Available" in slot['Usage']:
            slot_used = False
        else:
            slot_used = True
            # ToDo - Section not working. Check back later...
            # card_result = re.search('^[ ]*\w.+', slot['Card'])
            # try:
            #     card = card_result.group(0)
            # except Exception as e:
            #     card = None
            #     print("\n*******CARD*******\n%s\n" % e)
        slot_objects.append(SystemHW.PCIe(designation=slot['Designation'],
                                          slot_type=slot['Type'],
                                          card=card, used=slot_used))
    return slot_objects


def __bios__(motherboard_info, bios_settings):
    """
    BIOS Settings
       NOTE: Only available if SUM is activated

    Path Used:
      System -> BIOS Settings JSON -> Advanced|Boot Feature
      System -> BIOS Settings JSON -> Boot
      System -> BIOS Settings JSON -> Advanced|SATA Configuration
      System -> Components -> Motherboard

    Variables Used:               Expected (Example):
    BIOS Settings JSON
      - Power Button Function     "01              "
      - Wait For "F1" If Error    "01              "
      - Boot Mode Select          "02                     "
      - Dual Boot Order #(#)      "0005                "
      - Legacy Boot Order #(#)    "0003               "
      - UEFI Boot Order #(#)      "0001                 "
      - Configure SATA as         "01                    "
    Motherboard
      - Bios date                 "12/16/2016"
      - Bios                      "2.0"

    :param bios_settings: 
    :return: 
    """
    new_bios = SystemHW.BIOS()
    # -------------------------- BIOS Version and Date --------------------------
    new_bios.version = __check_for_result__(motherboard_info, ["Bios"])
    new_bios.version_date = __check_for_result__(motherboard_info, ["Bios date"])
    # -------------------------- Advanced Boot Feature --------------------------
    new_bios.sum_activated = __check_for_result__(bios_settings, ['Advanced|Boot Feature'], return_bool=True)
    if new_bios.sum_activated:
        # -------------------------- Boot Features --------------------------
        power_button = __check_for_result__(bios_settings, ['Advanced|Boot Feature', 'Power Button Function'])
        if power_button == 1:
            new_bios.instant_off = "Instant Off"
        else:
            new_bios.instant_off = "4 Second Delay"
        del power_button
        wait_f1 = __check_for_result__(bios_settings, ['Advanced|Boot Feature', 'Wait For "F1" If Error'])
        if wait_f1 == 1:
            new_bios.f1_wait = True
        else:
            new_bios.f1_wait = False
        del wait_f1
        # -------------------------- Boot --------------------------
        boot_mode = eval(str(__check_for_result__(bios_settings, ['Boot', 'Boot Mode Select', 'Value'])))
        if boot_mode == 0:
            new_bios.boot_mode = "Legacy"
            for i in range(1, 8):
                new_bios.boot_order.append(__decode_boot__(
                    bios_settings['Boot']['Legacy Boot Order #%d' % int(i)]['Value'],
                    "Legacy"))
        elif boot_mode == 1:
            new_bios.boot_mode = "UEFI"
            for i in range(1, 9):
                new_bios.boot_order.append(__decode_boot__(
                    bios_settings['Boot']['UEFI Boot Order #%d' % int(i)]['Value'], "UEFI"))
        elif boot_mode == 2:
            new_bios.boot_mode = "UEFI & Legacy (Dual)"
            for i in range(1, 16):
                new_bios.boot_order.append(__decode_boot__(
                    bios_settings['Boot']['Dual Boot Order #%d' % int(i)]['Value'], "Dual"))
        del boot_mode
        # -------------------------- Advanced SATA Configuration --------------------------
        sata_mode = __check_for_result__(
            bios_settings, ['Advanced|SATA Configuration', 'Configure SATA as', 'Value'])
        if sata_mode:
            sata_mode_key = ["IDE", "AHCI", "RAID"]
            new_bios.sata_controller_mode = sata_mode_key[eval(str(sata_mode))]
        else:
            sata_mode = __check_for_result__(
                bios_settings, ['Advanced|SATA Configuration', 'SATA Mode Selection', 'Value'])
            if sata_mode:
                sata_mode_key = ["AHCI", "RAID"]
                new_bios.sata_controller_mode = sata_mode_key[eval(str(sata_mode))]
    return new_bios


def __os__(os_info, new_server):
    """
    OS Information

    Path Used:
      System -> OS -> release
      System -> OS -> kernel

    Variables Used:               Expected (Example):
      - Kernel                    "3.10.0-327.28.3.el7.x86_64"
      - Release                   "CentOS Linux release 7.3.1611 (Core)"

    :param os_info: 
    :return: 
    """

    os = __check_for_result__(os_info, ['release'])
    if not os:
        new_server.os = __check_for_result__(os_info, ['Release'])
    else:
        new_server.os = os
    del os
    os_kernel = __check_for_result__(os_info, ['kernel'])
    if not os_kernel:
        new_server.os_kernel = __check_for_result__(os_info, ['Kernel'])
    else:
        new_server.os_kernel = os_kernel


def __query_to_object__(query_result):
    """
    internal method to convert a query from rethinkdb to Server() class object
    :param query_result: single result of the rethinkdb query
    :return: returns a Server object, populated with information from the query_result variable.
    """
    print("Processing information for " + str(query_result['SM Number']))
    # -----------------------------Basic System Information-----------------------------
    print("SM Number......", end='')
    if query_result['SM Number']:
        # Creating a new server object and assigning the SM from teh current Query Result dict
        new_server = SystemHW.Server(query_result['SM Number'])
        print("[ OK ]")
        # Checking for Trogdor in the dictionary
        trogdor_ok = __check_for_result__(query_result, ['Trogdor'], return_bool=True)
        # Customer Name from Jarvis
        customer_name = __check_for_result__(query_result, ['Customer Name'])
        # Customer name from Trogdor
        trogdor_cn = __check_for_result__(query_result, ['Trogdor', 'Customer'])
        # if both returned results, check that they match
        if trogdor_cn and customer_name:
            new_server.customer = __check_match__(str(trogdor_cn), str(customer_name))
        # If trogdor variable cannot be accessed, use Customer Name
        elif __check_for_result__(query_result, ['Customer Name'], return_bool=True):
            new_server.customer = query_result['Customer Name']
        del customer_name, trogdor_cn
        # Date-time of report
        report_dt = __check_for_result__(query_result, ['Datetime'])
        if report_dt:
            new_server.report_date = report_dt
        del report_dt
        # -------------------------------------
        # Project Number
        #
        # Variables used:
        #  - jpn = Jarvis Project Number
        #  - tpn = Trogdor Project Number
        # -------------------------------------
        if trogdor_ok:
            jpn = __check_for_result__(query_result, ['Project Number'])
            tpn = __check_for_result__(query_result, ['Trogdor', 'Project'])
            if jpn and tpn:
                new_server.project = __check_match__(str(jpn), str(tpn))
            elif jpn:
                new_server.project = str(jpn)
            del tpn, jpn
        else:
            jpn = __check_for_result__(query_result, ['Project Number'])
            if jpn:
                new_server.project = str(jpn)
            del jpn
        # -------------------------------------
        # Order Number
        #
        # Variables Used:
        #  - jon = Jarvis Order Number
        #  - ton = Trogdor Order Number
        # -------------------------------------
        if trogdor_ok:
            jon = __check_for_result__(query_result, ['Order Number'])
            ton = __check_for_result__(query_result, ['Trogdor', 'Order Number'])
            if not ton:
                ton = __check_for_result__(query_result, ['Trogdor', 'Order'])
            if jon and ton:
                if '#' in jon:
                    jon = jon[1:]
                if '#' in ton:
                    ton = ton[1:]
                new_server.order = __check_match__(str(jon), str(ton))
            elif jon:
                new_server.order = str(jon)
            del ton, jon
        else:
            jon = __check_for_result__(query_result, ['Project Number'])
            if jon:
                new_server.order = str(jon)
            del jon
        # Check for and collect DMI Information
        if __check_for_result__(query_result, ['Components', 'Motherboard', 'DMI'], return_bool=True):
            new_server.dmi_information = __dmi__(query_result['Components']['Motherboard']['DMI'])
        elif __check_for_result__(query_result, ['Components', 'Motherboard', 'Dmi'], return_bool=True):
            new_server.dmi_information = __dmi__(query_result['Components']['Motherboard']['Dmi'])

        # Check for and collect FRU Information
        if __check_for_result__(query_result, ['Components', 'Motherboard', 'FRU'], return_bool=True):
            new_server.fru_information = __fru__(query_result['Components']['Motherboard']['FRU'])
        elif __check_for_result__(query_result, ['Components', 'Motherboard', 'Fru'], return_bool=True):
            new_server.fru_information = __fru__(query_result['Components']['Motherboard']['Fru'])

        # Check for and collect IPMI Information
        if __check_for_result__(query_result, ['Components', 'IPMI'], return_bool=True):
            __ipmi__(query_result['Components']['IPMI'], new_server)

        # Check for and collect NIC information
        if __check_for_result__(query_result, ['Components', 'Network Adapters'], return_bool=True):
            new_server.network = __nic__(query_result['Components']['Network Adapters'])

        # Check for and collect drive information
        if __check_for_result__(query_result, ['Components', 'Drives'], return_bool=True):
            new_server.drives = __drives__(query_result['Components']['Drives'])

        # Check for and collect RAM information
        if __check_for_result__(query_result, ['Components', 'RAM'], return_bool=True):
            new_server.ram = __ram__(query_result['Components']['RAM'])

        # Check for and collect CPU information
        if __check_for_result__(query_result, ['Components', 'CPUs'], return_bool=True):
            new_server.cpu = __cpu__(query_result['Components']['CPUs'])

        # Check for and collect PCI card and slot information
        if __check_for_result__(query_result, ['Components', 'PCI Slots'], return_bool=True):
            new_server.card_slots = __pci__(query_result['Components']['PCI Slots'])

        if __check_for_result__(query_result, ['Components', 'Motherboard'], return_bool=True) and \
                __check_for_result__(query_result, ['BIOS Settings JSON'], return_bool=True):
            new_server.bios = __bios__(query_result['Components']['Motherboard'],
                                       query_result['BIOS Settings JSON'])
            if new_server.bios.sum_activated:
                new_server.sum_activated = True
        if __check_for_result__(query_result, ['OS'], return_bool=True):
            __os__(query_result['OS'], new_server)
        return new_server


def run_query(sm_number="", project="", order="", __recurs__=False):
    # TODO add query to Trogdor db to verify valid project/order computer that runs query must
    # TODO (cont) be on both prod and office networks to pull from Trogdor and Jarvis
    """
    Given a project number or an order number, this will run a query against jarvis' RethinkDB. ONLY PASS PROJECT OR
    ORDER--not both. Passing both will result in the method returning False and no query will be run.
    
    :param sm_number: Serial number to run the query against
    :param project: project number that MUST conform to "######.#"
    :param order: order number that MUST conform to "A#######"
    :param __recurs__: Internal Variable used to see if the method was called recursively or not.
    
    :return: Returns a list of Server() objects, one for each SM in the query result
    """
    # Connection to db
    r.connect("jarvis.siliconmechanics.com", 28015).repl()
    # Input validation - ensuring that there is something passed into the function and that there is not both order and
    # project inputs.
    if not order and not project and not sm_number:
        return False
    else:
        if order is not "":
            # The following RE will be replaced by a Trogdor db query.
            if not re.findall('[A-Z][0-9]{6,7}', order):
                print("Did not march order pattern")
                return False
            # Initial query to get all system SM's in order or project
            print("Pattern Match to Order. Beginning Jarvis Query")
            cursor = r.db("production"). \
                table("logs"). \
                get_all(str(order), index='Order Number'). \
                pluck('SM Number').distinct().run()
            print("Initial query complete")
        elif sm_number is not "":
            cursor = r.db("production"). \
                table("logs"). \
                get_all(str(sm_number), index='SM Number'). \
                pluck('SM Number').distinct().run()
            print("Pattern matches existing SM Number")
        else:
            # The following RE will be replaced by a Trogdor db query.
            print("Pattern Match to Project. Beginning Jarvis Query")
            if not re.findall('[1-9][0-9]{4,5}\.[0-9]{1,2}', project):
                print("Did not match project pattern")
                return False
            cursor = r.db("production"). \
                table("logs"). \
                get_all(str(project), index='Project Number'). \
                pluck('SM Number').distinct().run()
            print("Initial query complete")
        # Creating a server list for Server() objects
        print("Number of results:{0}".format(len(cursor)))
        server_list = []
        for item in cursor:
            print("Querying information for " + str(item['SM Number']))
            # Querying each SM from previous to get most recent upload
            q2 = r.db('production'). \
                table('logs'). \
                get_all(str(item['SM Number']), index='SM Number'). \
                order_by(r.desc("IEEE Datetime")). \
                limit(1). \
                run()
            # Going through each result - should only be one result for each 2nd query
            for result in q2:
                new_obj = __query_to_object__(result)
                if new_obj:
                    server_list.append(new_obj)
        if len(cursor) == 0 and not __recurs__:
            second_run_results = run_query(project=project, order=("#" + order), __recurs__=True)
            if len(second_run_results) > 0:
                return second_run_results
    return server_list
