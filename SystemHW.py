class BIOS:
    def __init__(self):
        self.boot_mode = None
        self.boot_order = []
        self.f1_wait = None
        self.instant_off = None
        self.sata_controller_mode = None
        self.version = 0.0
        self.version_date = ""
        self.sum_activated = False


class DMI:
    def __init__(self, chassis='', superserver='', model=''):
        self.chassis_serial = chassis
        self.uuid = None
        self.superserver_serial = superserver
        self.superserver_model = model
        self.system_platform = None
        self.system_manufacturer = "Silicon Mechanics"
        self.sm_number = None
        self.asset_tag = None


class FRU:
    def __init__(self, mb_serial='', prod_name=''):
        self.chassis_serial = None
        self.superserver_serial = None
        self.superserver_model = None
        self.motherboard_serial = mb_serial
        self.motherboard_part_number = None
        self.motherboard_manufacturer = "Supermicro"
        self.motherboard_manufacturing_date = None
        self.product_manufacturer = "Silicon Mechanics"
        self.product_model_number = None
        self.product_name = prod_name
        self.product_version = None
        self.product_serial_number = None
        self.product_asset_tag = None


class Drive:
    def __init__(self, brand='', model='', serial='', size='', virtual=False, slot=None, path=None, controller=None):
        self.brand = brand
        self.serial = serial
        self.model = model
        self.capacity = size
        self.virtual = virtual
        self.slot = slot
        self.path = path
        self.controller = controller


class RaidSet:
    def __init__(self):
        self.raid_level = 0
        self.member_drives = []


class Nic:
    def __init__(self, mac="", nic_type="", nic_name="", nic_model="", ip=""):
        self.mac_address = mac
        self.interface_type = nic_type
        self.name = nic_name
        self.model = nic_model
        self.ip_address = ip


class PCIe:
    def __init__(self, designation='', slot_type='', card='', used=False):
        self.designation = designation
        self.type = slot_type
        self.card = card
        self.used = used


class RAM:
    def __init__(self, slot_no="", serial="", size="", model="", manufacturer="", connection_type="", speed=""):
        self.slot = slot_no
        self.serial = serial
        self.size = size
        self.model = model
        self.manufacturer = manufacturer
        self.type = connection_type
        self.speed = speed


class CPU:
    def __init__(self, model="", cores=0, speed="", socket=''):
        self.speed = speed
        self.cores = cores
        self.model = model
        self.socket = socket


class Server(object):
    customer = ""
    def __init__(self, sm):
        self.order = None
        self.project = None
        self.sum_activated = False
        self.sm_number = sm
        self.report_date = None
        self.ipmi_firmware = None
        self.ipmi_ip = None
        self.cpu = []
        self.ram = []
        self.total_drives = 0
        self.card_slots = []
        self.drives = []
        self.network = []
        self.os = None
        self.os_kernel = None
        self.dmi_information = DMI()
        self.fru_information = FRU()
        self.bios = BIOS()
